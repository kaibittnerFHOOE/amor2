C = [1e-3 0; 0 1e-3]/(2*pi);
G = [0 1; -1 0];
w=1/sqrt(det(C));
P=2*pi/w
dt = 1e-5;
t = 0:dt:5e-3;
T=2*P
s = [-1; 0] * max (w*(T-t),0) ;
x0 = [0; w*T];

len = size(s,2);
%start = round(T/dt)+1;
start = 1;

x = trapez_lin (2, dt, x0, C, G, s);

figure(1)
hold off; plot(t(start:len),x(1,start:len)); %hold on; plot(t(start:len),x(2,start:len),'k'); 
legend('u(t)','I(t)');
title('Trapezoidal rule');

order=2
x = bdf_lin (order, 2, dt, x0, C, G, s);

figure(2)
hold off; plot(t(start:len),x(1,start:len)); %hold on; plot(t,x(2,:),'k'); 
legend('u(t)','I(t)');
title('BDF');
