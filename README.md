# Executive Summary

Even though the wireless market is a rapidly developing area, it is necessary to have a good knowledge of both the theoretical foundations and the need to own / rent expensive measuring equipment.

Although the use of unlicensed frequency bands is free, there are many legal restrictions that allow you to communicate with other users of the same frequency band. These limits, for example, limit transmission power or occupied bandwidth. To achieve maximum efficiency (higher efficiency is associated with lower energy requirements), it is necessary to operate the components at the limit of physical capabilities. Before placing any equipment on the market, compliance with all requirements shall be demonstrated in the test facility.

Companies are therefore faced with the challenge of designing radio circuits, especially those operating in the high-frequency range. 
Circuit design, together with legal knowledge, requires special know-how and expensive measuring instruments, which creates a considerable financial burden for start-ups, SMEs. One option is either to rent equipment or to purchase third party services and know-how. However, buying all the necessary tools is usually a high financial burden for a small business. However, buying all the necessary tools is usually a high financial burden for a small business.

The modeling platform provides possibility to measure basic radio parameters at an affordable price and, as well the creation of mathematical models that can be used in the development process in simulation programs (e.g. Octave / Matlab, Simulink, Python, AWR Microwave Office or Keysight ADS).
